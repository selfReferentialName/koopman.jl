using CairoMakie
using Koopman
using DynamicalSystems
using PredefinedDynamicalSystems
using LinearAlgebra
using Random
import Logging

baseLogger = Logging.global_logger()
struct MyLogger <: Logging.AbstractLogger
end
Logging.handle_message(L :: MyLogger, v, m, M, g, i, f, l; kwargs...) = Logging.handle_message(baseLogger, v, m, M, g, i, f, l; kwargs...)
Logging.shouldlog(L :: MyLogger, v, _module, g, i) = _module != Koopman && Logging.shouldlog(baseLogger, v, _module, g, i)
Logging.min_enabled_level(L :: MyLogger) = Logging.min_enabled_level(baseLogger)
Logging.global_logger(MyLogger())

# assumes models[1] is the full order model
function plotTraj(models :: Vector{Tuple{String,Any}}, x0, t)
	@assert length(models) > 0

	xs = [Matrix{eltype(complex.(x0))}(undef, size(x0,1), t) for model in models]
	for i=1:length(models)
		simulate!(models[i][2], x0, 1, xs[i])
	end
	xs = [abs.(x) for x in xs]

	# truncate very bad trajectories
	idrop = 0
	for i=2:length(models)
		i -= idrop
		x = xs[i]
		xt = xs[1]
		for t=1:size(x,2)
			if log10(norm(x[:,t] - xt[:,t])) > 1
				if t == 1
					@error models[i][1] "immediately has too much error"
					models = models[1:length(models) .!= i]
					xs = xs[1:length(xs) .!= i]
					idrop = idrop + 1
					break
				else
					@warn models[i][1] "has large absolute error" t
					xs[i] = x[:,1:t-1]
					break
				end
			end
		end
	end

	fig = Figure(resolution=(1920,1080))
	if length(x0) == 1
		ax1 = Axis(fig[1,1], title="Trajectory over Time", xlabel="Time", ylabel="Position")
		for i=1:length(models)
			lines!(ax1, 1:t, vec(xs[i]), label=models[i][1])
		end
		ax2 = Axis(fig[2,1], title="Error against "*models[1][1], xlabel="Time", ylabel="Log10 absolute error")
		for i=2:length(models)
			if i == 2
				lines!(ax2, 1:size(xs[i],2), log10.([norm(xs[i][:,j]-xs[1][:,j]) for j=1:size(xs[i],2)]), alpha=0.0) # skip a color
			end
			lines!(ax2, 1:size(xs[i],2), log10.([norm(xs[i][:,j]-xs[1][:,j]) for j=1:size(xs[i],2)]))
		end
		Legend(fig[1:2,2], ax1, "Models")
	elseif length(x0) == 2
		ax1 = Axis(fig[1,1], title="Trajectory", xlabel=L"x_1", ylabel=L"x_2")
		for i=1:length(models)
			scatter!(ax1, xs[i][1,:], xs[i][2,:], label=models[i][1], markersize=9+length(models)*2-i*2)
		end
		ax2 = Axis(fig[2,1], title="Error against "*models[1][1], xlabel="Time", ylabel="Log10 absolute error")
		for i=2:length(models)
			if i == 2
				lines!(ax2, 1:size(xs[i],2), log10.([norm(xs[i][:,j]-xs[1][:,j]) for j=1:size(xs[i],2)]), alpha=0.0) # skip a color
			end
			lines!(ax2, 1:size(xs[i],2), log10.([norm(xs[i][:,j]-xs[1][:,j]) for j=1:size(xs[i],2)]))
		end
		for i=1:2
			axi = Axis(fig[i,2], title="Component " * string(i), xlabel="time")
			for j=1:length(models)
				lines!(axi, 1:size(xs[j],2), xs[j][i,:], label=models[j][1], markersize=9+length(models)*2-j*2)
			end
		end
		Legend(fig[1:2,3], ax1, "Models")
	elseif length(x0) == 3
		ax1 = Axis3(fig[1:3,1], title="Trajectory", xlabel=L"x_1", ylabel=L"x_2")
		for i=1:length(models)
			scatter!(ax1, xs[i][1,:], xs[i][2,:], xs[i][3,:], label=models[i][1], markersize=9+length(models)*2-i*2)
		end
		ax2 = Axis(fig[4:6,1], title="Error against "*models[1][1], xlabel="Time", ylabel="Log10 absolute error")
		for i=2:length(models)
			if i == 2
				lines!(ax2, 1:size(xs[i],2), log10.([norm(xs[i][:,j]-xs[1][:,j]) for j=1:size(xs[i],2)]), alpha=0.0) # skip a color
			end
			logerr = log10.(max.([norm(xs[i][:,j]-xs[1][:,j]) for j=1:size(xs[i],2)], 1e-15))
			@assert typeof(logerr) == Vector{Float64}
			lines!(ax2, logerr)
		end
		for i=1:3
			axi = Axis(fig[2i-1:2i,2], title="Component " * string(i), xlabel="time")
			for j=1:length(models)
				lines!(axi, 1:size(xs[j],2), xs[j][i,:], label=models[j][1], markersize=9+length(models)*2-j*2)
			end
		end
		Legend(fig[1:6,3], ax1, "Models")
	else
		ax1 = Axis(fig[1,1], title="Error against "*models[1][1], xlabel="Time", ylabel="Log10 absolute error")
		for i=2:length(models)
			lines!(ax1, 1:size(xs[i],2), log10.([norm(xs[i][:,j]-xs[1][:,j]) for j=1:size(xs[i],2)]), label=models[i][1])
		end
		Legend(fig[1:2,2], ax1, "Models")
	end
	return fig
end

function testDMD(f, X, Y, x0, t, Φ, filename)
	@debug "Matrix sizes" size(X) size(Y) size(x0)
	models = [("True", f), ("DMD", DMD(X, Y)), ("EDMD", EDMD(X, Y, Φ)), ("REDMD by EDMD", REDMD(EDMD(X, Y, Φ))),
		("REDMD by SVD", REDMD(X, Y, Φ))]
	#println(size(models[3][2].K.Z))
	save(filename, plotTraj(models, x0, t))
	println("Saving to ", filename)
end
function testDMD(f, X, x0, t, Φ, filename)
	Y = similar(X)
	for i=1:size(Y,2)
		Y[:,i] = f(X[:,i])
	end
	testDMD(f, X, Y, x0, t, Φ, filename)
end
function testDMD(system :: DynamicalSystem, t, Φ, filename; Δt=1.)
	function f(x)
		mysis = system
		set_state!(mysis, x)
		step!(mysis)
		return get_state(mysis)
	end
	sys = system
	Z = nothing
	if Δt == 1.
		Z,_ = trajectory(sys, t)
	else
		Z,_ = trajectory(sys, t, Δt=Δt)
	end
	Z = Matrix(Z)'
	@info filename "Lyapunov exponents"=lyapunovspectrum(system,10000)
	testDMD(f, Z[:,1:end-1], Z[:,2:end], get_state(system), t, Φ, filename)
end
function testDMD(A :: Matrix, X, x0, t, Φ, filename)
	println(size(A), size(X))
	Y = A * X
	testDMD(x->A*x, X, Y, x0, t, Φ, filename)
end

@time testDMD(qr(rand(2,2)).Q-1e-2I, rand(2,100), rand(2), 100, quadDictionary(2), "linear.png")
@time testDMD(qr(rand(10,10)).Q-1e-2I, rand(10,100), rand(10), 100, quadDictionary(10), "linear-big.png")
@time testDMD(PredefinedDynamicalSystems.duffing(), 100, quadDictionary(2), "duffing.png")
@time testDMD(PredefinedDynamicalSystems.henon(), 100, quadDictionary(2), "henon.png")
@time testDMD(PredefinedDynamicalSystems.logistic(r=3.1), 100, quadDictionary(1), "logistic.png")
#@time testDMD(PredefinedDynamicalSystems.lorenz(), 1000, quadDictionary(3), "lorenz.png")
@time testDMD(PredefinedDynamicalSystems.vanderpol(), 1000, quadDictionary(2), "vanderpol.png")

# basic SIR model, assumes population is 1, all in one space, and recovery is permanent
function sir_rule!(dx, x, p, t)
	β = p[1] # rate of infection
	γ = p[2] # rate of recovery
	dx[1] = -β*x[1]*x[2] # succeptable become infected
	dx[2] = β*x[1]*x[2] - γ*x[2] # succeptable become infected and infected recover
	dx[3] = γ*x[2] # infected recover
	return nothing
end
sir_model = CoupledODEs(sir_rule!, [1.0-1., 1., 0.0], [2., 0.5]) # rough parameters of "Spanish" flu
testDMD(sir_model, 3000, quadDictionary(3), "SIR.png")
# fix R = 1. - S - I
function sir_rule!(dx, x, p, t)
	β = p[1] # rate of infection
	γ = p[2] # rate of recovery
	dx[1] = -β*x[1]*x[2] # succeptable become infected
	dx[2] = β*x[1]*x[2] - γ*x[2] # succeptable become infected and infected recover
	return nothing
end
# https://commons.wikimedia.org/wiki/File:SIR-Modell.svg
sir_model = CoupledODEs(sir_rule!, [0.997, 0.003], [0.4, 0.04])
@time testDMD(sir_model, 100, quadDictionary(2), "SIR2.png")

function snke_rule!(dx, x, p, t)
	λ = p[1]
	μ = p[2]
	dx[1] = -μ*x[1]
	dx[2] = λ*(x[2]-x[1]^2)
	return nothing
end
snke_model = CoupledODEs(snke_rule!, [0.5,0.5], [-2.0,1.0])
@time testDMD(snke_model, 100, quadDictionary(2), "snke-quad.png")
@time testDMD(snke_model, 100, CachelessDictionary([x->x[1],x->x[2],x->x[1]^3], [1. 0. 0. ; 0. 1. 0.]), "snke-cube.png")

# pendulum with a bit of friction
function pendulum_rule!(dx, x, p, t)
	ℓ = p[1] # arm length
	g = p[2] # force of gravity
	μ = p[3] # coefficient of friction times normal force
	dx[1] = x[2] # accelleration becomes velocity
	dx[2] = -g/ℓ * sin(x[1]) + μ # fall by gravity, slow by friction
	return nothing
end
pendulum_model = CoupledODEs(pendulum_rule!, [3., 0.], [1., 10., 1.5])
@time testDMD(pendulum_model, 3000, quadDictionary(2), "pendulum.png")

# simple model which converges to 0 sublinearly
# x(t) = \frac{1}{\sqrt{c+2t}}
function cubed_rule!(dx, x, p, t)
	dx[1] = -x[1]^3
	return nothing
end
cubed_model = CoupledODEs(cubed_rule!, [0.75], [])
@time testDMD(cubed_model, 3000, quadDictionary(1), "cubed.png")

# simple model which converges to 0 sublinearly
# x(t) = \frac{1}{c+t}
function squared_rule!(dx, x, p, t)
	dx[1] = -x[1]^2
	return nothing
end
squared_model = CoupledODEs(squared_rule!, [0.75], [])
# @time testDMD(squared_model, 3000, quadDictionary(1), "squared.png") # takes forever

# simple model which converges to 0 superlinearly
# x(t) = \frac{2\sqrt{2}}{3\sqrt{3}} (c-t)^{\frac{2}{3}}
function cbrt_rule!(dx, x, p, t)
	dx[1] = -sign(x[1])*(abs(x[1])^(1/3))
	return nothing
end
cbrt_model = CoupledODEs(cbrt_rule!, [0.75], [])
@time testDMD(cbrt_model, 500, quadDictionary(1), "cbrt.png")

function normByOrder(X, Y, ks=1:100)
	x = zeros(length(ks))
	y = zeros(length(ks))
	for i=1:length(ks)
		try
			Φ = laguerreDictionary(size(X,1), ks[i])
			x[i] = kdim(Φ)
			K = EDMD(X, Y, Φ)
			y[i] = norm(K)
		catch e
			if i > 1
				x[i] = x[i-1]
				y[i] = y[i-1]
				break
			end
		end
	end
	fig = Figure(resolution=(1920,1080))
	ax = Axis(fig[1,1], title="Variation of estimated Koopman operator norm", xlabel="Lifted dimension", ylabel="2-Norm")
	lines!(ax, x, y)
	return fig
end
function normByOrder(f :: Function, X, ks=1:100)
	Y = similar(X)
	for i=1:size(Y,2)
		Y[:,i] = f(X[:,i])
	end
	return normByOrder(X, Y, ks)
end
function normByOrder(system :: DynamicalSystem, t, ks=1:100; Δt=1.)
	function f(x)
		mysis = (system)
		set_state!(mysis, x)
		step!(mysis)
		return get_state(mysis)
	end
	sys = deepcopy(system)
	Z = nothing
	if Δt == 1.
		Z,_ = trajectory(sys, t)
	else
		Z,_ = trajectory(sys, t, Δt=Δt)
	end
	Z = Matrix(Z)'
	return normByOrder(Z[:,1:end-1], Z[:,2:end], ks)
end

# save("squared-norms.png", normByOrder(squared_model, 3000))
save("sir-norms.png", normByOrder(sir_model, 3000))

function svdOfDmd(system :: DynamicalSystem, t, Φ, filename; Δt=1.)
	function f(x)
		mysis = system
		set_state!(mysis, x)
		step!(mysis)
		return get_state(mysis)
	end
	sys = system
	Z = nothing
	if Δt == 1.
		Z,_ = trajectory(sys, t)
	else
		Z,_ = trajectory(sys, t, Δt=Δt)
	end
	Z = Matrix(Z)'
	@info filename "Lyapunov exponents"=lyapunovspectrum(system,10000)
	X = Z[:,1:end-1]
	Y = Z[:,2:end]
	Kl = Matrix(DMD(X, Y, rtol=1e-13))
	Ke = Matrix(EDMD(X, Y, Φ, rtol=1e-13))
	Σl = svdvals(Kl)
	Σe = svdvals(Ke)
	Σl = Σl[Σl .> 1e-15*first(Σl)]
	Σe = Σe[Σe .> 1e-15*first(Σe)]
	fig = Figure(resolution=(1920,1080))
	ax = Axis(fig[1,1], title="Decay of Singular Values", xlabel="Singular Value Index", ylabel="log10(Singular Value)")
	lines!(ax, 1:length(Σl), log10.(Σl), label="DMD")
	lines!(ax, 1:length(Σe), log10.(Σe), label="EDMD")
	Legend(fig[1,2], ax, "Methods")
	save(filename, fig)
end

@time svdOfDmd(PredefinedDynamicalSystems.duffing(), 100, laguerreDictionary(2,20), "duffing-svd.png")
@time svdOfDmd(PredefinedDynamicalSystems.henon(), 100, laguerreDictionary(2,20), "henon-svd.png")
@time svdOfDmd(PredefinedDynamicalSystems.logistic(r=3.1), 100, laguerreDictionary(1,40), "logistic-svd.png")
@time svdOfDmd(PredefinedDynamicalSystems.lorenz(), 1000, laguerreDictionary(3,20), "lorenz-svd.png")
@time svdOfDmd(PredefinedDynamicalSystems.vanderpol(), 1000, laguerreDictionary(2,20), "vanderpol-svd.png")

# TODO: fix
function spectrumByN(system :: DynamicalSystem, dict, filename, steps=10000; tpf=1., fps=60.)
	return
	function f(x)
		mysis = (system)
		set_state!(mysis, x)
		step!(mysis)
		return get_state(mysis)
	end
	Z = nothing
	if tpf == 1.
		Z,_ = trajectory(system, steps+1)
	else
		Z,_ = trajectory(system, steps+1, Δt=tpf)
	end
	Z = Matrix(Z)'
	fig = Figure(resolution=(1920,1080))
	ax = Axis(fig[1,1], title="Change in spectrum of estimated K", xlabel="Real Part", ylabel="Imaginary Part")
	points = Observable(Point2f([0.,0.]))
	plt = scatter!(ax, points)
	limits!(ax, -2, 2, -2, 2)
	record(fig, filename, framerate=fps) do io
		for i=1:steps
			println(size(Z))
			K = EDMD(Z, dict)
			Λ = nothing
			if K.K isa Matrix
				Λ = eigvals(K.K)
			else
				Λ = diag(K.K.Λ)
			end
			λr = real.(Λ)
			λi = imag.(Λ)
			points[] = [Point2f(λr[i],λi[i]) for i=1:length(λr)]
			recordframe!(io)
		end
	end
end

spectrumByN(sir_model, laguerreDictionary(2,3), "sir-spec.gif")
