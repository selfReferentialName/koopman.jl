using Test
using Koopman
using Random
using LinearAlgebra

function eigsort(λs)
	lt(ζ, ξ) = real(ζ) < real(ξ) && imag(ζ) < imag(ξ)
	return sort(λs, lt=lt)
end

# setup code not to time

# basic DMD test
A = qr(rand(10,10)).Q
X = qr(complex.(randn(10,10000))).Q
κX = cond(X)
Y = A * X

Abig = qr(rand(200,200)).Q
Xbig = qr(randn(200,1000)).Q
Ybig = Abig * Xbig

cosF(x) = [cos(x[1])]
cosX = rand(1,1000)
cosY = cos.(cosX)

@testset showtiming=true "Koopman.jl tests" begin
	@testset "DMD (linear system)" begin
		K = DMD(X, Y)
#		println(abs.(eigsort(diag(K.Λ)) - eigsort(eigvals(A))))
#		@test eigsort(diag(K.Λ)) ≈ eigsort(eigvals(A))
		@test K.Zi * K.Z ≈ I
		@test cond(K.Λ) ≈ 1.
		@test cond(K.Z * K.Λ  * K.Zi) ≈ 1.
		@test K.Z * K.Λ * K.Zi ≈ A
		@test Y ≈ simulate(K,X,1)
	end
	@testset "DMD (big linear system)" begin
		K = DMD(Xbig, Ybig)
		@test K.Zi * K.Z ≈ I
		@test cond(K.Λ) ≈ 1.
		@test cond(K.Z * K.Λ  * K.Zi) ≈ 1.
		@test K.Z * K.Λ * K.Zi ≈ Abig
		@test Ybig ≈ simulate(K,Xbig,1)
	end
	@testset "Naive DMD (linear system)" begin
		K = DMD(X, Y, method=:pinv)
		@test K.Zi * K.Z ≈ I
		@test cond(K.Λ) ≈ 1.
		@test cond(K.Z * K.Λ  * K.Zi) ≈ 1.
		@test K.Z * K.Λ * K.Zi ≈ A
		@test K.Z * K.Λ * K.Zi * X[:,1] ≈ simulate(K,X[:,1],1)
		@test K.Z * K.Λ * K.Zi * X ≈ simulate(K,X,1)
		@test Y ≈ simulate(K,X,1)
	end
	@testset "Quadratic observables evaluation" begin
		dict = quadDictionary(3)
		x = [-1., 2., 3.]
		y = unique(sort(evalDict(dict, x)))
		@test [1., 2., 4.] ≈ sort(evalDict(quadDictionary(1), [2.]))
		@info (quadDictionary(2).select)
		@test evalDict(CachelessDictionary([x->1.0, x->x[1], x->x[2],
				x->x[1]^2, x->x[1]*x[2], x->x[2]^2], nothing), [7., 3.]) ≈
			evalDict(quadDictionary(2), [7., 3.])
		@test [-2., -1., 1., 2., 4.] ≈ unique(sort(evalDict(quadDictionary(2), [-1., 2.])))
		@test y ≈ [-3., -2., -1., 1., 2., 3., 4., 6., 9.]
	end
	@testset "EDMD (2x2 linear system)" begin
		local A = [0.2 0.8 ; 0.7 0.3]
		local X = rand(2, 3000)
		local Y = A * X
		K = EDMD(X, Y, quadDictionary(2))
		println(K)
		@test Y ≈ simulate(K,X,1)
	end
	@testset "EDMD (linear system)" begin
		K = EDMD(X, Y, quadDictionary(10))
		@test Y ≈ simulate(K,X,1)
	end
	@testset "Analytic EDMD (scaling system)" begin
		K = EDMD(x->π*x, quadDictionary(1), [(-1.,1.)], [100])
	end
	@testset "Analytic EDMD vs Pen and Paper" begin
		K = EDMD(cosF, quadDictionary(1), [(0.,1.)], [1000], dict_ortho=false)
		# "pen and paper" (mostly Wolfram Alpha) L^2 projection transpose
		Kp = [1. sin(1.) (2+sin(2.))/4. ; 0.5 sin(1)+cos(1)-1. (1+2sin(2.)+cos(2))/8 ; 1/3. 2cos(1)-sin(1) (4+3sin(2)+6cos(2))/24]
		@test all(isfinite.(K.K))
		@test K.K ≈ Kp atol=1e-5
	end
	@testset "EDMD on many points vs Analytic EDMD" begin
		Ka = EDMD(cosF, quadDictionary(1), [(0.,1.)], [1000])
		Ke = EDMD(cosX, cosY, quadDictionary(1))
		@test Ka.K ≈ Ke.K.Z*Ke.K.Λ*Ke.K.Zi atol=1e-3
		for i=1:100
			@test simulate(Ka,rand(3),10) ≈ simulate(Ke,rand(3),10) atol=1e-3 skip=true
		end
		@test simulate(Ka,cosX,1) ≈ simulate(Ke,cosX,1) atol=1e-5
	end
	#@testset "laguerreDictionary runs" begin
	#	K = EDMD(X, Y, laguerreDictionary(size(X,1),5))
	#	@test all(isfinite.(K.K.Z))
	#	@test all(isfinite.(K.K.Λ))
	#	@test all(isfinite.(K.K.Zi))
	#end
end