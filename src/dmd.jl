using LinearAlgebra
using FastGaussQuadrature

#include("dictionaries.jl")

# TODO: optional print statements

# general purpose DMD implementation
# takes input-output data into Λ, Z, and Zi. Λ eigenvalues, Z right eigenvectors, and Zi left eigenvectors
function dmd_schmid(X, Y; verbose=true :: Bool, rtol=1e-3 :: Float64)
	@assert size(X) == size(Y)
	# algorthm 2 of A LAPACK implementation of the Dynamic Mode Decomposition I (by Drmac)
	Dx = Diagonal(X)
	if size(X,1) > size(X,2) # underconstrained
		X = X / Dx # preconditioning
		Y = Y / Dx
	else # overconstrianed
		X = X
		Y = Y
	end
	U, Σ, V = svd(X)
	k = 1
	k = rank(diagm(Σ), rtol)
	if k==size(Σ,1) && verbose
		@info "Selecting full rank" rank=k
	elseif k+1 <= length(Σ) && verbose
		@info "Selecting rank" rank=k Σ[k+1], Σ[k]
	end
	@assert k > 0
	U = U[:,1:k]
	V = V[:,1:k]
	Σ = Σ[1:k]
	B = Y * (V / Diagonal(Σ))
	S = U' * B # Reighleigh quotient
	Λ, Z = eigen(S)
	if verbose
		@info "Schmid dmd calculated" ritz_values=Λ ρ=max(abs.(Λ)...)
	end
	@assert all(isfinite, Λ)
	@assert all(isfinite, Z)
	@assert all(isfinite, V)
#	@assert cond(Z) < 1e10 # commenting this out is a bad idea
	return (Λ, U*Z, Z\U')
end

# K ≈ Z Λ Zi
struct DMD{T}
	Λ :: Diagonal{T}
	Z :: Matrix{T}
	Zi :: Matrix{T}
	function DMD(Λ :: Diagonal{T}, Z :: Matrix{T}, Zi :: Matrix{T}) where {T}
		@assert size(Λ,1) == size(Z,2)
		@assert size(Z) == size(Zi')
		return new{T}(Λ, Z, Zi)
	end
end
DMD(λs :: Vector, Z :: Matrix, Zi :: Matrix) = DMD(Diagonal(λs), Z, Zi)
function DMD(X :: AbstractMatrix, Y :: AbstractMatrix; method=:schmid :: Symbol, verbose=true :: Bool, rtol=1e-3 :: Float64) :: DMD
	@assert size(X) == size(Y)
	if method == :schmid
		Λ, Z, Zi = dmd_schmid(X, Y, verbose=verbose, rtol=rtol)
		return DMD(Λ, Z, Matrix(Zi))
	elseif method == :pinv
		K = Y / X
		Λ, Z = eigen(K)
		return DMD(Λ, Z, inv(Z)) # would need to restructure to avoid inv
	else
		error("Unknown DMD method ", method)
	end
end
function simulate(model :: DMD, x0 :: AbstractVector, t :: Int) :: Vector
	@assert size(model.Z,1) == length(x0)
	return model.Z * (model.Λ.^t) * (model.Zi * x0)
end
function simulate(model :: DMD, X :: AbstractMatrix, t :: Int) :: Matrix
	@assert size(model.Λ,1) == size(X,1)
	return model.Z * (model.Λ.^t) * (model.Zi * X)
end
function Matrix(dmd :: DMD)
	return dmd.Z * dmd.Λ * dmd.Zi
end

struct EDMD{T}
	K :: Union{DMD{T},Matrix{T}}
	Φ :: Dictionary # observables
	function EDMD(K :: DMD{T}, Φ :: Dictionary) where {T}
		@assert size(K.Z,1) == kdim(Φ)
		return new{T}(K, Φ)
	end
	function EDMD(K :: Matrix{T}, Φ :: Dictionary) where {T}
		@assert size(K,1) == kdim(Φ)
		@assert size(K,1) == size(K,2)
		return new{T}(K, Φ)
	end
end
# approximate Koopman operator and do some housekeeping
function EDMD(X, observables; naive=false)
	n = kdim(observables) # dimension of assumed invariant subspace of observables with full state
	Xo = evalDict(observables, X)
	Λ, Z, Zi = dmd_schmid(Xo[:,1:end-1], Xo[:,2:end])
	return EDMD(DMD(Λ, Z, Zi), observables)
end
function EDMD(X, Y, observables; naive=false, rtol=1e-3)
	@assert size(X) == size(Y)
	Xo = evalDict(observables, X)
	Yo = evalDict(observables, Y)
	return EDMD(DMD(Xo, Yo, rtol=rtol), observables)
end
# Analytic DMD. dict_ortho assumes dictionary basis is orthonormal
function EDMD(f :: Function, observables :: Dictionary, quadPoints :: AbstractMatrix{Float64}, quadWeights :: AbstractVector{Float64};
		dict_ortho=false)
	dotL2(x,y) = dot(x.*y, quadWeights)
	Y = similar(quadPoints)
	for i=1:size(quadPoints, 2)
		Y[:,i] = f(quadPoints[:,i])
	end
	Yo = evalDict(observables, Y)
	Mμ = I
	Mf = zeros(kdim(observables), kdim(observables))
	Φμ = evalDict(observables, quadPoints)
	for i=1:kdim(observables)
		for j=1:kdim(observables)
			Mf[i,j] = dotL2(Yo[i,:], Φμ[j,:])
		end
	end
	if !dict_ortho
		Mμ = similar(Mf)
		for i=1:kdim(observables)
			for j=1:kdim(observables)
				Mμ[i,j] = dotL2(Φμ[i,:], Φμ[j,:])
			end
		end
	end
	A = Mf / Mμ
	return EDMD(Matrix(A'), observables)
end
# Analytic EDMD again, with Gaussian Quatrature, weight function described by weight
# :uniform means w(x) = 1/m(Ω)
# TODO: won't work in over 1 dimension
function EDMD(f :: Function, observables :: Dictionary, dims :: AbstractVector{Tuple{Float64,Float64}}, points :: AbstractVector{Int};
		dict_ortho=false, weight=:uniform)
	lerp(t, a, b) = t.*a + (1.0.-t).*b
	quadPoints = zeros(length(dims), max(points...))
	quadWeights = zeros(sum(points))
	if weight == :uniform
		for i=1:length(dims)
			quadPoints[i,1:points[i]], quadWeights[(i-1)*points[1]+1:i*points[1]] = gausslegendre(points[i])
			quadPoints[i,:] = lerp(quadPoints[i,:], dims[i][1], dims[i][2])
		end
	else
		error("Unknown L2 weight")
	end
	return EDMD(f, observables, quadPoints, quadWeights, dict_ortho=dict_ortho)
end
function simulate(model :: EDMD, x0 :: AbstractVector, t :: Int) :: Vector
	if model.K isa DMD
		return getState(model.Φ, simulate(model.K, evalDict(model.Φ, x0), t))
	else
		y = evalDict(model.Φ, x0)
		for i=1:t
			y = model.K * y
		end
		return getState(model.Φ, y)
	end
end
function simulate!(model :: EDMD, x0 :: AbstractVector, stride :: Int, out :: Matrix)
	y = evalDict(model.Φ, x0)
	for i=1:size(out,2)
		if model.K isa DMD
			y = simulate(model.K, y, stride)
		else
			for i=1:stride
				y = model.K * y
			end
		end
		out[:,i] = getState(model.Φ, y)
	end
end
function Matrix(edmd :: EDMD)
	if edmd.K isa DMD
		return Matrix(edmd.K)
	else
		return edmd.K
	end
end

struct REDMD{T}
	W :: Matrix{T}
	dict :: Dictionary
	function REDMD(W :: Matrix{T}, dict :: Dictionary) where {T}
		@assert size(W,2) == kdim(dict)
		return new{T}(W, dict)
	end
end
function REDMD(base :: EDMD)
	return REDMD(getState(base.Φ, base.K.Z)*base.K.Λ*base.K.Zi, base.Φ)
end
function REDMD(X, Y, observables, method=:svd, tol=1e-13)
	@assert size(X) == size(Y)
	Xo = evalDict(observables, X)
	U,Σ,V = svd(Xo)
	for k=2:length(Σ)
		if Σ[k]/Σ[1] < tol
			U = U[:,1:(k-1)]
			Σ = Σ[1:(k-1)]
			V = V[:,1:(k-1)]
			break
		end
	end
	@error size(V), size(Σ), size(U)
	return REDMD(Y * V * (Diagonal(Σ) \ U'), observables)
end
function simulate(model :: REDMD, x0 :: AbstractVector, t :: Int) :: Vector
	@assert size(model.W,1) == length(x0)
	x = x0
	for i=1:t
		x = model.W * evalDict(model.dict, x)
	end
	return x
end
