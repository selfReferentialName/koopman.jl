abstract type Dictionary end

function Base.findlast(arr :: AbstractVector, val)
	for i=1:length(arr)
		if arr[i] == val
			return i
		end
	end
end
function Base.findlast(arr :: AbstractMatrix, val :: AbstractVector)
	for i=1:size(arr,2)
		if Vector(arr[:,i]) == Vector(val)
			return i
		end
	end
end

function evalDict(Φ :: Dictionary, X :: AbstractMatrix) :: Matrix
	Y = Matrix{eltype(X)}(undef, kdim(Φ), size(X,2))
	for i=1:size(X,2)
		Y[:,i] = evalDict(Φ, X[:,i])
	end
	return Y
end
function getState(Φ :: Dictionary, X :: AbstractMatrix) :: Matrix
	return hcat([getState(Φ, X[:,i]) for i=1:size(X,2)]...)
end

struct CachelessDictionary <: Dictionary
	ϕs :: Vector{Function}
	B :: Union{Matrix,Nothing}
end
kdim(Φ :: CachelessDictionary) = length(Φ.ϕs)
function evalDict(Φ :: CachelessDictionary, x :: AbstractVector) :: Vector
	return [f(x) for f in Φ.ϕs]
end
select(Φ :: CachelessDictionary, S :: Vector{Int}) = CachelessDictionary(Φ.ϕs[S], Φ == nothing ? nothing : Φ.B[:,S])
function getState(Φ :: CachelessDictionary, y :: AbstractVector)
	if Φ.B == nothing
		error("Dictionary does not include full state!")
	end
	return Φ.B * y
end

struct AdjoinStateDictionary <: Dictionary
	base :: Dictionary
	n :: Int
	Π :: Vector{Int}
end
AdjoinStateDictionary(base, n) = AdjoinStateDictionary(base, n, Vector(1:n+kdim(base)))
kdim(Φ :: AdjoinStateDictionary) = length(Φ.Π)
function evalDict(Φ :: AdjoinStateDictionary, x :: AbstractVector)
	y = evalDict(Φ.base, x)
	return [Π[i] <= Φ.n ? x[Π[i]] : y[Π[i]] for i=1:length(Φ.Π)]
end
function select(Φ :: AdjoinStateDictionary, S :: Vector{Int}) :: AdjoinStateDictionary
	base = select(Φ.base, [i-Φ.n for i in S if i > Φ.n])
	S = [S[i] > Φ.n ? S[i] + Φ.n : S[i] for i=1:length(S)]
	return AdjoinStateDictionary(base, Φ.n, Φ.select[Φ.select-k in Φ.base])
end
function getState(Φ :: AdjoinStateDictionary, y :: AbstractVector)
	vecpreimag(x, z) = [findlast(x, ye) for ye in y]
	if 1:Φ.n ⊆ Φ.Π
		return y[vecpreimag(Φ.Π, 1:Φ.n)]
	else
		error("Dictionary does not include full state!", "It was selected out.")
	end
end

struct AdjoinConstantDictionary <: Dictionary
	base :: Dictionary
	idx :: Int
end
kdim(Φ :: AdjoinConstantDictionary) = kdim(Φ) + 1
function evalDict(Φ :: AdjoinConstantDictionary, x :: AbstractVector)
	y = evalDict(Φ.base, x)
	return vcat([y[i] for i=1:Φ.idx], [1], [y[i-1] for i=Φ.idx+1:kdim(Φ)])
end
function select(Φ :: AdjoinConstantDictionary, S :: Vector{Int}) :: Dictionary
	k = kdim(Φ.base)
	base = select(Φ.base, S[S <= k])
	if k+1 in S
		return AdjoinConstantDictionary(base, findlast(S,k+1))
	else
		return base
	end
end
getState(Φ :: AdjoinConstantDictionary, y) = getState(Φ.base, y)

struct OrthopolyDictionary <: Dictionary
	α :: Function
	β :: Function
	γ :: Function
	shape :: Vector{Int}
	select :: Matrix{Int}
	function OrthopolyDictionary(α, β, γ, shape :: Vector{Int})
		function cartMat(args...)
			println(length(args), ", ", prod(length.(args)))
			out = Matrix{eltype(args[1])}(undef, length(args), prod(length.(args)))
			for i=1:size(out,2)
				ri = i
				for j=1:size(out,1)
					out[j,i] = args[j][ri % length(args[j]) + 1]
					ri = ri ÷ length(args[j])
				end
			end
			return out
		end
		select = cartMat([0:shape[i] for i=1:length(shape)]...)
#		println(select)
		return new(α, β, γ, shape, select)
	end
	function OrthopolyDictionary(α :: Function, β :: Function, γ :: Function, nvars :: Integer, order :: Integer)
		@assert nvars >= 1
		select = Matrix{Int16}(undef, nvars, sum([binomial(nvars+ord-1, ord) for ord=0:order]))
		select[:,1] = repeat([Int16(0)], nvars)
		idx = 2
		for ord = 1:order
			entry = repeat([1], ord) # entry in the ord order symmetric nvars dim tensor we represent
			done = false
			while !done
				for i=1:nvars
					select[i,idx] = sum(entry .== i)
				end
				idx += 1
				for i=ord:-1:0
					if i == 0
						done = true
						break
					end
					entry[i] += 1
					if entry[i] > nvars || (i > 1 && entry[i] > entry[i-1])
						entry[i] = 1
						continue
					else
						break
					end
				end
			end
		end
		return new(α, β, γ, repeat([order], nvars), select)
	end
	function OrthopolyDictionary(α, β, γ, select)
		shape = [max(a...) for a in select]
		return new(α, β, γ, shape, select)
	end
	function OrthopolyDictionary(Φ :: OrthopolyDictionary, select)
		return OrthopolyDictionary(Φ.α, Φ.β, Φ.γ, select)
	end
end
kdim(Φ :: OrthopolyDictionary) = size(Φ.select,2)
function evalDict(Φ :: OrthopolyDictionary, x :: AbstractVector) :: Vector
	cache = Vector{Vector{eltype(x)}}(undef, length(Φ.shape))
	for i=1:size(cache,1)
		cache[i] = Vector{eltype(x)}(undef, Φ.shape[i]+1)
		cache[i][1] = 1
		@assert isfinite(Φ.α(0))
		@assert isfinite(Φ.β(0))
		cache[i][2] = Φ.α(0)*x[i] + Φ.β(0)
		for j=1:Φ.shape[i]-2+1
			cache[i][j+2] = (Φ.α(j)*x[i] + Φ.β(j)) * cache[i][j+1] + Φ.γ(j) * cache[i][j]
		end
	end
#	println(cache)
	y = Vector{eltype(x)}(undef, kdim(Φ))
	for i=1:size(Φ.select,2)
		y[i] = 1.
		for j=1:length(Φ.select[:,i])
			if Φ.select[j,i] == 0 # sub in one
			else
				y[i] = y[i] * cache[j][Φ.select[j,i] + 1]
			end
		end
	end
	return y
end
select(Φ :: OrthopolyDictionary, S :: Vector{Int}) = OrthopolyDictionary(Φ, Φ.select[S])
function getState(Φ :: OrthopolyDictionary, y :: AbstractVector)
	n = length(Φ.shape)
	x = Vector{eltype(y)}(undef, n)
	for i=1:n
		j = findlast(Φ.select, [k==i ? 1 : 0 for k=1:n])
		if j === nothing
			error("Dictionary does not include full state!", "It was selected out.", "Missing index: ", i)
		end
		x[i] = y[j]
	end
	return x
end

function quadDictionary(n)
	return OrthopolyDictionary(x->1.0, x->0.0, x->0.0, n, 2)
end
function laguerreDictionary(n, d)
	return OrthopolyDictionary(j -> -1.0/(j+2), j -> (2j+3)/(j+2), j -> (-j-1)/(j+2), n, d)
end