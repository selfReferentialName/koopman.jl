# get norm of estimated koopman operator
norm(K :: DMD) = norm(K.Z * K.K * K.Zi)
norm(K :: EDMD) = norm(K.K)