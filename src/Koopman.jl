module Koopman

#general
export simulate, simulate!

# dictionaries.jl
export Dictionary, evalDict, kdim, select, getState
export CachelessDictionary, AdjoinConstantDictionary, AdjoinStateDictionary
export OrthopolyDictionary, quadDictionary, laguerreDictionary
export norm

# dmd.jl
export DMD, EDMD, REDMD

include("dictionaries.jl")
include("dmd.jl")

function simulate(model, X0 :: AbstractMatrix, t :: Int) :: Matrix
	Y = similar(X0)
	for i=1:t
		Y[:,i] = simulate(model, X0[:,i], t)
	end
	return Y
end
function simulate!(model, x0 :: AbstractVector, stride :: Int, out :: Matrix)
	x = x0
	for i=1:size(out,2)
		out[:,i] = simulate(model, x, stride)
		x = out[:,i]
	end
end

function simulate(f :: Function, x0 :: AbstractVector, t :: Int)
	x = x0
	for i=1:t
		x = f(x)
	end
	return x
end

greet() = print("Hello World!")

end # module Koopman
