using Koopman
using Random
using LinearAlgebra
using BenchmarkTools

function randMatAndInvWithCond(n :: Int, cond :: Float64, norm=1. :: Float64)
	U = qr(randn(n,n)).Q
	V = qr(randn(n,n)).Q
	Σ = Diagonal(rand(Float64, n)*(norm-norm/cond) .+ norm/cond)
	return (U*Σ*V', V*Σ\U')
end

function randMatWithEigAnnulus(n :: Int, ρinv :: Float64, ρ :: Float64, eigcond=1.0 :: Float64)
	function randEig()
		λ = randn() + randn()*im
		m = rand(Float64) * (ρ-ρinv) + ρinv
		return m * λ/abs(λ)
	end
	V, Vinv = randMatAndInvWithCond(n, eigcond)
	Λ = Diagonal([randEig() for i=1:n])
	return V * Λ * Vinv
end

A = randMatWithEigAnnulus(100, 0.1, 0.9, 10.0)
X = randn(100, 1000)
Y = A*X

println(show(@benchmark DMD(X, Y, verbose=false)))
K = DMD(X, Y)

println(show(@benchmark simulate(K, X, 1)))